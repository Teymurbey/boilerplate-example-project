@startuml
note "'origin' is site we used for fetching data" as N1
class Comment {
    +Number id
    +String comment
    +Date date
    +User user
    +Boolean status
    +Statement statement
    +Date createdAt
    +Date updatedAt
}
Comment o-> User
Comment o-down-> Statement
enum UserRoles {
    FACILITY_MANAGER
    CHAIN_MANAGER
    REGULAR
}
class User {
    +Number id
    +String login
    +String password
    +Chain chain
    +UserRoles role
    +Date createdAt
    +Date updatedAt
}
User o-> Chain

class Chain {
    +String name
}

enum StatementStatuses {
    NEEDS_REVIEW
    NEEDS_FIX
    FULLY_PAID
    DO_NOT_BILL
    DELIVERED
    PROCESSED_FOR_DELIVERY
    REROUTED
    RETURNED
    PRINTED_LOCALLY
    RETURNED_TO_SENDER
    IN_PROCESS
    IN_TRANSIT
    IN_LOCALAREA
    REVIEWED
    STOP_BILLING
    DO_NOT_MAILOUT
    READY_TO_BILL
    EXTANDED
}

Class Statement {
    +Number id
    +String number
    +Facility facility
    +Residnet resident
    +Number billed
    +Number paid
    +Number outstanding
    +Number billingDate
    +Date dateMailed
    +Boolean processed
    +String ncoa
    +String endorse
    +String barcode
    +String imbEncode
    +String pdf
    +String img
    +StatementStatuses status
    +Date createdAt
    +Date updatedAt
}
Statement o-down-> Facility
Statement o-down-> Resident


Class Transaction {
    +Statement statement
    +Resident resident
    +Facility facility
    +String payer
    +String description
    +Number unitAmount
    +String numberOfUnits
    +Number amount
    +String type
    +String chargeCode
    +String revenueCode
    +String checkNumber
    +String batchNumber
    +String careLevel
    +String benefitDays
    +String hcpcs
    +String mppr
    +String glAccounts
    +String daysAccounts
    +String billed
    +Date postingDate
    +Date effectiveDate
    +Date billingDate
    +Date createdAt
    +Date updatedAt
}

Transaction o-> Facility
Transaction o-> Resident
Transaction o-> Statement

class Facility {
    +Number id
    +String name
    +String companyName
    +Boolean deleted
    +Date datePulled
    +String contactPerson
    +String email
    +String phone
    +String website
    +String logo
    +String location
    +String addressLob
    +String hospiceMedicaid
    +String medicaidPending
    +String origin
    +String description
    +Chain chain
    +User[] users
    +Date createdAt
    +Date updatedAt
}
Facility o-> User
Facility o-> Chain

class FacilityAddress {
    +String line1
    +String line2
    +String line3
    +String street
    +String city
    +String state
    +String zip
    +Facility facility
}
FacilityAddress o-> Facility

class Resident {
    +Number id
    +Facility facility
    +ResidentCollection[] collections
    +String firstName
    +String lastName
    +ResidentStatus status
    +Date moveInDate
    +String unit
    +String floor
    +String room
    +String bed
    +String origin 
    +Date createdAt
    +Date updatedAt
}
Resident o-down-> Facility
Resident o-> ResidentCollection

enum ResidentStatus {
    ACTIVE
    INACTIVE
}

class ResidentContact {
    +Number id
    +Resident resident
    +String typeofContact
    +String nameTitle
    +String firstName
    +String lastName
    +String relationship
    +String addressLine1
    +String addressLine2
    +String addressLine3
    +String city
    +String state
    +String zipCode
    +String homePhone
    +String homePhonePriority
    +String officePhone
    +String officePhonePriority
    +String mobilePhone
    +String mobilePhonePriority
    +String email
    +String origin
    +Boolean isUsedForMailing
    +Date createdAt
    +Date updatedAt
}
ResidentContact o-> Resident

class LobLetter {
    +Number id
    +String pdf
    +String id
    +String trackingNumber
    +String description
    +Object data
    +Statement statement
}
note left: data is full letter data from lob
LobLetter o-down-> Statement

enum ResidentCollectionStatus {
    NEW
    PENDING
    ESCALETED
    CALL_LETTER
}

class ResidentCollection {
    +Number id
    +String type
    +ResidentCollectionStatus status
    +User[] users
    +Date statusUpdatedAt
    +Date createdAt
    +Date updatedAt
}

class ExternalSitesResidents {
    +Number id
    +String originId
    +String origin
    +Resident resident
}
ExternalSitesResidents o-down-> Resident

class ExternalSitesFacilities {
    +Number id
    +String origin
    +String originId
    +String origin
    +String login
    +Facility facility
    +Date createdAt
    +Date updatedAt
}
ExternalSitesFacilities o-> Facility

class ResidentSettings {
    +Number id
    +Boolean stopSendingStatements
    +Boolean deliverToRoom
    +Boolean canBeAddedToCollections
    +Resident resident
    +Date createdAt
    +Date updatedAt
}

ResidentSettings o-down-> Resident

class FacilitySettings {
    +Number id
    +Boolean reviewStatementsBeforeMailing
    +Facility facility
    +Date createdAt
    +Date updatedAt
}

FacilitySettings o-down-> Facility

@enduml